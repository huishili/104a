/* Partner: Aishni Parab (aparab@ucsc.edu)
 Partner: Hui Shi Li (huli@ucsc.edu)
*/

// Use cpp to scan a file and print line numbers.
// Print out each input line read in, then strtok it for
// tokens.

#include <string>
using namespace std;
#include <iostream>
#include <errno.h>
#include <libgen.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wait.h>
#include <unistd.h>
#include "auxlib.h"
#include "string_set.h"
#include "astree.h"
#include "lyutils.h"
#include <cstdio>

const string CPP = "/usr/bin/cpp -nostdinc";
constexpr size_t LINESIZE = 1024;
std::string dopt = ""; 
FILE* tokfile = NULL;
FILE* strfile = NULL;
FILE* astfile = NULL;

// Chomp the last character from a buffer if it is delim.
void chomp (char* string, char delim) {
  size_t len = strlen (string);
  if (len == 0) return;
  char* nlpos = string + len - 1;
  if (*nlpos == delim) *nlpos = '\0';
}

/* Scans for options
   Returns idx after options-start idx of fn
*/
int scan_opts (int argc, char** argv) {
  // opterr = 0;
  yy_flex_debug = 0;
  yydebug = 0;
  //bool badopt;
  int argi = 1;
  //optarg- everything after the opt
  for(; argi < argc ; argi++) {
    int opt = getopt (argc, argv, "@:D:l:y");
    if (opt == EOF) break;
      //char opt_ascii = opt;
      switch (opt) {
        case '@': 
            //cout << "optarg: "<< optarg << endl;
          set_debugflags (optarg);   break;
        case 'D':
            // cout << "d flag: "<< opt_ascii << endl;
            // cout << "d flag: " << optarg << endl;
          dopt = "-D" + string(optarg);
            //cout << "dopt: " << dopt << endl;
          break;
        case 'l': 
           yy_flex_debug = 1;
          break;
        case 'y': yydebug = 1;
          break;
        default:  fprintf (stderr, "%s: %d\n",
            "bad option", optopt);
          break;
      }
  }
  return argi;
}

/* Checks for valid .oc extension 
   Returns pos/neg value if not equal, 0 if equal
*/
bool check_ext (char* filename) {
  string fn = string(basename(filename));
  size_t last_idx = fn.find_last_of(".");
  string ext = fn.substr(last_idx);
  string oc_ext = ".oc";
  return strcmp(ext.c_str(), oc_ext.c_str());
}

/* Removes .oc extension from filename
   Returns filename without the ext
*/
string remove_ext(char* filename) {
  string fn = string(basename(filename));
  size_t lastidx = fn.find_last_of(".");
  return fn.substr(0,lastidx);
}

int main (int argc, char** argv) {
  exec::execname = basename(argv[0]);
  int exit_status = EXIT_SUCCESS;
  int file_idx = scan_opts(argc, argv);
  int argi = file_idx;
  char* filename = argv[argi];
  
  string outfile_tok = remove_ext(filename) + ".tok";
  tokfile = fopen(outfile_tok.c_str(), "w");
  
  /* Scans through filenamesgiven
     Opens files and calls cpplines for each file
  */
  for (; argi < argc; ++argi) {
    //char* filename = argv[argi];
    if(check_ext(filename) != 0) {
      exit_status = EXIT_FAILURE;
      fprintf (stderr, "%s: %s\n",
      "Invalid filename extension", basename(filename));
      continue;
    }

    string command = CPP + " " + dopt + " " + filename;
    DEBUGF('d', "command=\"%s\"\n", command.c_str());
    
    yyin = popen(command.c_str(), "r");
    if (yyin == NULL) {
      syserrprintf (command.c_str());
      exit(exec::exit_status);
    }else {
      if(yy_flex_debug){
        fprintf(stderr, "im in yy_flex_debug, stderr");
        fprintf (stderr, "--popen (%s), fileno(yyin) = %d\n",
          command.c_str(), fileno(yyin));
      } 
      
      
      int parse_rc = yyparse();
      fclose(tokfile);

      int pclose_rc = pclose(yyin);
      eprint_status (command.c_str(), pclose_rc);
      if (pclose_rc != 0) exec::exit_status = EXIT_FAILURE;
      yylex_destroy();

      if (yydebug or yy_flex_debug) {
        fprintf(stderr, "%s\n", "debug is on");
        fprintf (stderr, "Dumping paser::root:\n");
        if (parser::root != nullptr) parser::root->dump_tree(stderr);
        fprintf(stderr, "Dumping string_set:\n");
        string_set::dump(stderr);
      }
      if(parse_rc) {
        errprintf ("parse failed {%d}\n", parse_rc);
      } else {
      //astree::print(stdout, parser::root);
       // emit_sm_code (parser::root);
        delete parser::root;         
      }


    }
  }
    
  /* Remove .oc ext and add .str ext
  Then write to the new .str file
  */
  string str_fn = remove_ext(filename) + ".str";
  string wr = "w";
  strfile = fopen(str_fn.c_str(), wr.c_str());
  string_set::dump(strfile);
  fclose(strfile);

  string ast_fn = remove_ext(filename) + ".ast";
  astfile = fopen(ast_fn.c_str(), wr.c_str());
  astree::print(astfile, yyparse_astree, 0);
  fclose(astfile);
  return exit_status;
}

