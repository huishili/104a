//Hui Shi Li, huli@ucsc.edu
//Aishni Parab, aparab@ucsc.edu

%{
#include <cassert>

#include "lyutils.h"
#include "astree.h"

%}

%debug
%defines
%error-verbose
%token-table
%verbose

%token TOK_STRING TOK_CHAR TOK_INT TOK_VOID 
%token TOK_STRUCT TOK_WHILE TOK_RETURN TOK_IF TOK_ELSE  
%token TOK_NEW TOK_ARRAY TOK_NULL
%token TOK_POS TOK_NEG TOK_TYPEID TOK_FIELD
%token TOK_IDENT TOK_INTCON TOK_STRINGCON TOK_CHARCON
%token TOK_ROOT TOK_NEWARRAY TOK_RETURNVOID
%token TOK_BLOCK TOK_IFELSE TOK_CALL
%token TOK_NEWSTRING TOK_FUNCTION TOK_PARAMLIST TOK_PROTOTYPE
%token TOK_DECLID TOK_VAR TOK_INDEX
%token TOK_EQ TOK_NE TOK_LE TOK_GE TOK_LT TOK_GT

%right TOK_IF TOK_ELSE
%right '='
%left  TOK_EQ TOK_NE TOK_LT TOK_LE TOK_GT TOK_GE
%left  '+' '-'
%left  '*' '/' '%'
%right TOK_POS TOK_NEG '!' TOK_NEW
%left  TOK_ARRAY TOK_FIELD TOK_FUNCTION

%start start

%%

start     : program             { yyparse_astree = $1; }
          ;

program   : program struct   { $$ = $1->adopt($2); 
                                //printf("%s\n", "struct");
                                }
          | program function    { $$ = $1->adopt($2);
                                //printf("%s\n", "function");
                                }
          | program statement   { $$ = $1->adopt($2); 
                                //printf("%s\n", "statment");
                                }
          | program error '}'   { $$ = $1; }
          | program error ';'   { $$ = $1; }
          |                     { $$ = new_parseroot(); }
          ;

structstruct : '{' field ';'
              {
                  destroy($3);
                  $$ = $1->adopt($2);
              }
            | structstruct field ';'
              {
                  destroy($3);
                  $$ = $1->adopt($2);
              }
            ;

struct  : TOK_STRUCT TOK_IDENT structstruct '}'
              {
                  destroy($4);
                  $2 = $2->adopt_sym($2, TOK_TYPEID);
                  $$ = $1->adopt($2, $3);
              }
            | TOK_STRUCT TOK_IDENT '{' '}'
              {
                  destroy($3, $4);
                  $2 = $2->adopt_sym($2, TOK_TYPEID);
                  $$ = $1->adopt($2);
              }
            ;

field   : basetype TOK_IDENT
              {
                  $2 = $2->adopt_sym($2, TOK_FIELD);
                  $$ = $1->adopt($2);
              }
            | basetype TOK_ARRAY TOK_IDENT
              {
                  $3 = $3->adopt_sym($3, TOK_FIELD);
                  $$ = $2->adopt($1, $3);
              }
            ;

basetype    : TOK_VOID   { $$ = $1; }
            | TOK_CHAR   { $$ = $1; }
            | TOK_INT    { $$ = $1; 
            //printf("%s\n", "inside basetype");
            }
            | TOK_STRING { $$ = $1; }
            | TOK_IDENT  { $$ = $1->adopt_sym($1, TOK_TYPEID); }
            ;

params      : '(' ident
              {
                  $1 = $1->adopt_sym($1, TOK_PARAMLIST);
                  $$ = $1->adopt($2);                  
                  //printf("%s\n", "inside params"); 
              }
            | params ',' ident
              {
                  destroy($2);
                  $$ = $1->adopt($3);
              }
            ;

function    : ident params ')' block
              {
                  destroy($3);
                  $$ = new astree(TOK_FUNCTION, $1->lloc, "");
                  $$ = $$->adopt($1, $2);
                  $$ = $$->adopt($4);
                  //printf("%s\n", "inside function"); 
              }
            | ident params ')' ';'
              {
                  destroy($3, $4);
                  $$ = new astree(TOK_PROTOTYPE, $1->lloc, "");
                  $$ = $$->adopt($1, $2);
              }
            | ident '(' ')' block
              {
                  destroy($3);
                  $2 = $2->adopt_sym($2, TOK_PARAMLIST);
                  $$ = new astree(TOK_FUNCTION, $1->lloc, "");
                  $$ = $$->adopt($1, $2);
                  $$ = $$->adopt($4);
              }
            | ident '(' ')' ';'
              {
                  destroy($3, $4);
                  $2 = $2->adopt_sym($2, TOK_PARAMLIST);
                  $$ = new astree(TOK_PROTOTYPE, $1->lloc, "");
                  $$ = $$->adopt($1, $2);
              }
            ;

ident   : basetype TOK_IDENT
              { 
                  //$2->dump_node(stdout);
                  $2 = $2->adopt_sym($2, TOK_DECLID);
                  $$ = $1->adopt($2);
                  //printf("%s\n", "inside ident"); 
              }
            | basetype TOK_ARRAY TOK_IDENT
              { 
                  $3 = $3->adopt_sym($3, TOK_DECLID);
                  $$ = $2->adopt($1, $3);
              }
            ;

body        : '{' statement
              { 
                  //$1->dump_node(stdout);
                  $1 = $1->adopt_sym($1, TOK_BLOCK);
                  $$ = $1->adopt($2);
                  //printf("%s\n", "inside body, one stmt"); 
              }
            | body statement
              { 
                  $$ = $1->adopt($2);
                  //$2->dump_node(stdout);
                  //printf("%s\n", "inside body, multiple smt");  
              }
            ;

block       :  body '}'
              {
                  destroy($2);
                  $$ = $1->adopt_sym($1, TOK_BLOCK);
                  //printf("%s\n", "inside block"); 
              }
            | '{' '}'
              { 
                  destroy($2);
                  $$ = $1->adopt_sym($1, TOK_BLOCK);
              }
            ;

statement   : block    { $$ = $1; }
            | var  { $$ = $1; 
            //printf("%s\n", "inside stmt, var");
            }
            | while   { $$ = $1; 
            //printf("%s\n", "inside stmt, while");
            }
            | ifelse   { $$ = $1; }
            | return   { $$ = $1; }
            | expr ';' 
              {
                  destroy($2);
                  $$ = $1;
              }
            | ';'      { $$ = $1; }
            ;

var         : ident '=' expr ';'
              { 
                  destroy($4);
                  $2 = $2->adopt_sym($2, TOK_VAR);
                  $$ = $2->adopt($1, $3);
                  //printf("%s\n", "inside var"); 
              }
            ;

while       : TOK_WHILE '(' expr ')' statement
              { 
                  destroy($2, $4);
                  $$ = $1->adopt($3, $5);
                  //printf("%s\n", "inside while");
              }
            ;

ifelse      : TOK_IF '(' expr ')' statement TOK_ELSE statement
              {
                  destroy($2, $4);
                  $1->adopt_sym($1, TOK_IFELSE);
                  $$ = $1->adopt($3, $5);
                  $$ = $$->adopt($7);
              }
            | TOK_IF '(' expr ')' statement
              {
                  destroy($2, $4);
                  $$ = $1->adopt($3, $5);
              }
            ;

return      : TOK_RETURN ';'
              {
                  destroy($2);
                  $$ = $1->adopt_sym($1, TOK_RETURNVOID);
              }
            | TOK_RETURN expr ';'
              { 
                  destroy($3);
                  $$ = $1->adopt($2);
              }
            ;

expr        : expr binop expr { $$ = $2->adopt($1, $3); }
            | unop expr       { $$ = $$->adopt($2); }
            | allocator       { $$ = $1; }
            | call            { $$ = $1; }
            | '(' expr ')'    
              {
                   destroy($1, $3);
                   $$ = $2;
              }
            | variable        { $$ = $1; }
            | constant        { $$ = $1; }
            ;

binop       : TOK_EQ          { $$ = $1; }
            | TOK_NE          { $$ = $1; }
            | TOK_LT          { $$ = $1; }
            | TOK_LE          { $$ = $1; }
            | TOK_GT          { $$ = $1; }
            | TOK_GE          { $$ = $1; }
            | '+'             { $$ = $1; }
            | '-'             { $$ = $1; }
            | '*'             { $$ = $1; }
            | '/'             { $$ = $1; }
            | '='             { $$ = $1; }
            ;

unop        : TOK_POS         { $$ = $1; }
            | TOK_NEG         { $$ = $1; }
            | '!'             { $$ = $1; }
            | TOK_NEW         { $$ = $1; }
            ;

allocator   : TOK_NEW TOK_IDENT '(' ')'
              {
                  destroy($3, $4);
                  $2 = $2->adopt_sym($2, TOK_TYPEID);
                  $$ = $1->adopt($2);
              }
            | TOK_NEW TOK_STRING '(' expr ')'
              {
                  destroy($3, $5);
                  $1 = $1->adopt_sym($1, TOK_NEWSTRING);
                  $$ = $1->adopt($4);
              }
            | TOK_NEW basetype '[' expr ']'
              {
                  destroy($3, $5);
                  $1 = $1->adopt_sym($1, TOK_NEWARRAY);
                  $$ = $1->adopt($2, $4);
              }
            ;

cexprs      : TOK_IDENT '(' expr
              {
                  $2 = $2->adopt_sym($2, TOK_CALL);
                  $$ = $2->adopt($1, $3);
              }
            | cexprs ',' expr
              { 
                  destroy($2);
                  $$ = $1->adopt($3);
              }
            ;

call        : cexprs ')'
              {
                  destroy($2);
                  $$ = $1;
              }
            | TOK_IDENT '(' ')'
              {
                  destroy($3);
                  $2 = $2->adopt_sym($2, TOK_CALL);
                  $$ = $2->adopt($1);
              }        
            ;

variable    : TOK_IDENT          { $$ = $1; }
            | expr '[' expr ']'  
              { 
                  destroy($4);
                  $2 = $2->adopt_sym($2, TOK_INDEX);
                  $$ = $2->adopt($1, $3);
              }
            | expr '.' TOK_IDENT 
              {
                  $3 = $3->adopt_sym($3, TOK_FIELD);
                  $$ = $2->adopt($1, $3);
              }
            ;

constant    : TOK_INTCON         { $$ = $1; }
            | TOK_CHARCON        { $$ = $1; }
            | TOK_STRINGCON      { $$ = $1; }
            | TOK_NULL           { $$ = $1; }
            ;

%%

const char *parser::get_tname(int symbol) {
   return yytname [YYTRANSLATE(symbol)];
}
